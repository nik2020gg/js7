function filterBy(arr, dataType) {
  return arr.filter((item) => typeof item !== dataType);
}

const inputArray = ["hello", "world", 23, "23", null];
const dataTypeToFilter = "string";

const resultArray = filterBy(inputArray, dataTypeToFilter);

console.log(resultArray);

//forEach - це оператор для обходу елементів у колекції
//$arr=null
//isArray()
